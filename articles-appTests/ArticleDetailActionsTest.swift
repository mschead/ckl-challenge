//
//  ArticleDetailActionsTest.swift
//  articles-appTests
//
//  Created by Marcos Schead on 18/12/17.
//  Copyright © 2017 Marcos Schead. All rights reserved.
//

import XCTest
@testable import articles_app
import ReSwift

class ArticleDetailActionsTest: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    func testSetArticleDetailAction() {
        let article = ArticleWrapper(title: "article1", website: "website1", authors: "author1", date: "10/10/2010", content: "content1", tags: [], image_url: "url1", markRead: false)
        let image = UIImage(named: "icon-example")

        mainStore.dispatch(SetArticleDetailAction(article, image!))
        let state = mainStore.state.articleDetailState

        XCTAssert(state.title == article.title)
        XCTAssert(state.authors == article.authors)
        XCTAssert(state.content == article.content)
        XCTAssert(state.date == article.date)
        XCTAssert(state.image == image)
        XCTAssert(state.website == article.website)
    }

    
}
