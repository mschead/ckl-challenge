//
//  ArticleListingActionsTest.swift
//  articles-appTests
//
//  Created by Marcos Schead on 18/12/17.
//  Copyright © 2017 Marcos Schead. All rights reserved.
//

import XCTest
@testable import articles_app
import ReSwift

class ArticleListingActionsTest: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    func testChangeSortOrderAction() {
        mainStore.state.articleListingState.sortArrowUp = false

        mainStore.dispatch(ChangeSortOrderAction())

        XCTAssert(mainStore.state.articleListingState.sortArrowUp == true)
    }
    
    func testSortArticleByAction() {
        mainStore.state.articleListingState.sortType = SortType.author
        
        mainStore.dispatch(SortArticleByAction(SortType.title))
        
        XCTAssert(mainStore.state.articleListingState.sortType == SortType.title)
    }
    
}
