//
// Created by Marcos Schead on 15/12/17.
// Copyright (c) 2017 Marcos Schead. All rights reserved.
//

import Foundation
import UIKit

struct ArticleDetailState {

    var title = String()
    var website = String()
    var authors = String()
    var date = String()
    var content = String()
    var image: UIImage?

}