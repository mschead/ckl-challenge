//
// Created by Marcos Schead on 15/12/17.
// Copyright (c) 2017 Marcos Schead. All rights reserved.
//

import Foundation
import ReSwift

struct ArticleDetailReducer: Reducer {

    typealias ReducerStateType = ArticleDetailState

    func handleAction(action: Action, state: ArticleDetailState?) -> ArticleDetailState {
        if state == nil {
            return ArticleDetailState()
        }

        switch action {
        case let action as SetArticleDetailAction:
            return setArticle(state: state!, action: action)
        default:
            return state!
        }
    }

    fileprivate func setArticle(state: ArticleDetailState, action: SetArticleDetailAction) -> ArticleDetailState {
        var state = state

        let article = action.article

        state.title = article.title
        state.authors = article.authors
        state.content = article.content
        state.date = article.date
        state.image = action.image
        state.website = article.website

        return state
    }

}