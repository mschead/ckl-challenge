//
// Created by Marcos Schead on 15/12/17.
// Copyright (c) 2017 Marcos Schead. All rights reserved.
//

import Foundation
import ReSwift

struct SetArticleDetailAction : Action {

    let article : ArticleWrapper
    let image: UIImage

    init(_ article: ArticleWrapper, _ image: UIImage) {
        self.article = article
        self.image = image
    }

}
