//
//  ArticleDetailViewController.swift
//  articles-app
//
//  Created by Marcos Schead on 15/12/17.
//  Copyright © 2017 Marcos Schead. All rights reserved.
//

import UIKit
import ReSwift

class ArticleDetailViewController: UIViewController, StoreSubscriber {

    typealias StoreSubscriberStateType = ArticleDetailState

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var source: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainStore.subscribe(self, selector: {$0.articleDetailState})
    }

    func newState(state: ArticleDetailState) {
        image.image = state.image
        author.text = state.authors
        date.text = state.date
        content.text = state.content
        source.text = state.website
    }

}
