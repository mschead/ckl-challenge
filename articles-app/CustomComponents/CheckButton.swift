//
//  CheckButton.swift
//  articles-app
//
//  Created by Marcos Schead on 16/12/17.
//  Copyright © 2017 Marcos Schead. All rights reserved.
//

import UIKit

class CheckButton: UIButton {

    override func draw(_ rect: CGRect) {
        self.layer.borderWidth = CGFloat(floatLiteral: 0.5)
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.cornerRadius = CGFloat(floatLiteral: 5.0)
    }

}
