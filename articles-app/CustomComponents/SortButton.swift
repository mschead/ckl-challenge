//
//  SortButton.swift
//  articles-app
//
//  Created by Marcos Schead on 16/12/17.
//  Copyright © 2017 Marcos Schead. All rights reserved.
//

import UIKit

class SortButton: UIButton {

    override var isHighlighted: Bool {
        didSet {
            let colorDefault = UIColor(red: 37.0/255.0, green: 37.0/255.0, blue: 37.0/255.0, alpha: 1.0)
            backgroundColor = isHighlighted ? colorDefault : UIColor.black
        }
    }

}
