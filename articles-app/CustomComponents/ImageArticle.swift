//
// Created by Marcos Schead on 16/12/17.
// Copyright (c) 2017 Marcos Schead. All rights reserved.
//

import Foundation
import UIKit

class ImageArticle: UIImageView {

    override func layoutSubviews() {
        self.layer.borderWidth = CGFloat(floatLiteral: 0.5)
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.cornerRadius = CGFloat(floatLiteral: 5.0)
    }
}