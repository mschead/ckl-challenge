//
//  Article+CoreDataProperties.swift
//  articles-app
//
//  Created by Marcos Schead on 05/01/18.
//  Copyright © 2018 Marcos Schead. All rights reserved.
//
//

import Foundation
import CoreData


extension Article {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Article> {
        return NSFetchRequest<Article>(entityName: "Article")
    }

    @NSManaged public var authors: String?
    @NSManaged public var content: String?
    @NSManaged public var date: String?
    @NSManaged public var image_url: String?
    @NSManaged public var mark_read: Bool
    @NSManaged public var title: String?
    @NSManaged public var website: String?

}
