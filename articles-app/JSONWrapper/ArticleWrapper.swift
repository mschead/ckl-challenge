//
// Created by Marcos Schead on 15/12/17.
// Copyright (c) 2017 Marcos Schead. All rights reserved.
//

import Foundation


struct ArticleWrapper: Codable {

    var title: String
    var website: String
    var authors: String
    var date: String
    var content: String
    var tags: [TagWrapper]
    var image_url: String

    var markRead: Bool = false

    // Choose which attributes the codedable will map in the JSON
    enum CodingKeys: String, CodingKey {
        case title
        case website
        case authors
        case date
        case content
        case tags
        case image_url
    }

    // Just to simplify the challenge, but the correct would be use an ID or all the attributes
    static func ==(lhs: ArticleWrapper, rhs: ArticleWrapper) -> Bool {
        return lhs.title == rhs.title
    }

}

