//
// Created by Marcos Schead on 15/12/17.
// Copyright (c) 2017 Marcos Schead. All rights reserved.
//

import Foundation

struct TagWrapper: Codable {

    var id: Int
    var label: String

}