//
// Created by Marcos Schead on 15/12/17.
// Copyright (c) 2017 Marcos Schead. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ArticleListingAdapter: NSObject, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate {

    var tableView: UITableView
    var fetchedResultsController: NSFetchedResultsController<Article>

    init(tableView: UITableView) {
        self.tableView = tableView

        let request = NSFetchRequest<Article>(entityName: "Article")
        let titleSort = NSSortDescriptor(key: "title", ascending: true)
        request.sortDescriptors = [titleSort]

        let moc = NSManagedObjectContext.mr_default()
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
    }


    func initializeFetchedResultsController() {
        fetchedResultsController.delegate = self

        do {
            try fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }

    }

    // UITableViewDataSource and UITableViewDelegate methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.fetchedObjects!.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ArticleTableViewCell else {
            fatalError("The dequeued cell is not an instance of ArticleCell.")
        }

        let article = fetchedResultsController.fetchedObjects![indexPath.row]
        mainStore.dispatch(SetArticleDetailAction(ObjectMapper.NSObjectToArticle(object: article), cell.thumb.image!))
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "articleCell", for: indexPath) as? ArticleTableViewCell else {
            fatalError("The dequeued cell is not an instance of ArticleCell.")
        }

        let object = self.fetchedResultsController.object(at: indexPath)
        cell.setFieldValue(article: ObjectMapper.NSObjectToArticle(object: object))

        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections!.count
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultsController.sections else {
            fatalError("No sections in fetchedResultsController")
        }
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }


    // NSFetchedResultsControllerDelegate methods

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            break
        }
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }

    func sortItens(sortType: SortType, ascending: Bool) {
        let sorter = NSSortDescriptor(key: sortType.rawValue, ascending: ascending)
        fetchedResultsController.fetchRequest.sortDescriptors = [sorter]
        try! fetchedResultsController.performFetch()
        tableView.reloadData()
    }

}