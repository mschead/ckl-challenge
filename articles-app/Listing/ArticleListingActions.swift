//
// Created by Marcos Schead on 15/12/17.
// Copyright (c) 2017 Marcos Schead. All rights reserved.
//

import Foundation
import ReSwift


struct SortArticleByAction: Action {

    let sortType: SortType

    init(_ sortType: SortType) {
        self.sortType = sortType
    }

}

struct CheckButtonClickedAction: Action {

    let article: ArticleWrapper

    init(_ article: ArticleWrapper) {
        self.article = article
    }

}

struct ErrorMarkingReadAction: Action {}

struct ChangeSortOrderAction: Action {}

enum SortType : String {
    case title = "title"
    case author = "authors"
    case date = "date"
    case website = "website"
}