//
// Created by Marcos Schead on 15/12/17.
// Copyright (c) 2017 Marcos Schead. All rights reserved.
//

import Foundation

struct ArticleListingState {

    var sortArrowUp: Bool = true
    var sortType: SortType = SortType.title
    var errorMarkingRead: Bool = false

}