//
// Created by Marcos Schead on 15/12/17.
// Copyright (c) 2017 Marcos Schead. All rights reserved.
//

import Foundation
import UIKit

class ArticleTableViewCell: UITableViewCell {

    var article: ArticleWrapper!

    @IBOutlet weak var thumb: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var readButton: UIButton!

    @IBAction func readButton(_ sender: Any) {
        mainStore.dispatch(CheckButtonClickedAction(article))
    }
    
    func setFieldValue(article: ArticleWrapper) {
        self.article = article
        title.text = article.title
        readButton.setTitle(article.markRead ? "✓" : "✘", for: .normal)
        readButton.setTitleColor(article.markRead ? .green : .red, for: .normal)
        thumb.downloadImageFrom(link: article.image_url, contentMode: UIViewContentMode.redraw)
    }

}
