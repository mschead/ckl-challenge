//
//  ArticleListingViewController.swift
//  articles-app
//
//  Created by Marcos Schead on 15/12/17.
//  Copyright © 2017 Marcos Schead. All rights reserved.
//

import UIKit
import ReSwift
import CoreData
import MagicalRecord

class ArticleListingViewController: UIViewController, StoreSubscriber {

    typealias StoreSubscriberStateType = ArticleListingState

    @IBOutlet weak var articleTable: UITableView!
    var adapter: ArticleListingAdapter!

    @IBOutlet weak var arrowButton: UIButton!
    @IBOutlet weak var titleButton: UIButton!
    @IBOutlet weak var authorButton: UIButton!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var websiteButton: UIButton!
    @IBOutlet weak var sortHeader: UIView!
    
    @IBOutlet weak var indicatorActivity: UIActivityIndicatorView!
    @IBOutlet weak var tableLoadingLabel: UILabel!
    @IBOutlet weak var loadingView: UIView!

    // variable to check if a sort is needed, when newState gets called
    var sortingOnProcess = false

    override func viewDidLoad() {
        super.viewDidLoad()

        adapter = ArticleListingAdapter(tableView: articleTable)
        adapter.initializeFetchedResultsController()

        articleTable.delegate = adapter
        articleTable.dataSource = adapter

        loadTableInfo()
        mainStore.subscribe(self, selector: {$0.articleListingState})
        configureButtonsLayout()
    }

    func newState(state: ArticleListingState) {
        arrowButton.setTitle(state.sortArrowUp ? "↑" : "↓", for: UIControlState.normal)
        if (sortingOnProcess) {
            adapter.sortItens(sortType: state.sortType, ascending: state.sortArrowUp)
            sortingOnProcess = false
        }

        if (state.errorMarkingRead) {
            mainStore.state.articleListingState.errorMarkingRead = false
            showAlertMarkReadError()
        }

    }


    fileprivate func loadTableInfo() {
        let articlesCoreData = adapter.fetchedResultsController.fetchedObjects ?? []

        if articlesCoreData.isEmpty {
            ArticleRequest().makeRequest() { (result) in
                switch result {
                case .success(let articles):

                    articles.forEach({article in
                        ArticlePersistence().save(article: article, callbackError: self.showAlertSyncError)
                    })

                    self.loadingView.isHidden = true
                    self.sortHeader.isHidden = false
                    self.articleTable.isHidden = false

                case .failure(let error):
                    print("error: \(error.localizedDescription)")
                    self.indicatorActivity.isHidden = true
                    self.tableLoadingLabel.text = "Error fetching the articles. Try reopening the app!"
                }
            }
        } else {
            self.loadingView.isHidden = true
            self.sortHeader.isHidden = false
            self.articleTable.isHidden = false
        }

    }
    
    fileprivate func configureButtonsLayout() {
        titleButton.backgroundColor = UIColor.black
        titleButton.layer.borderWidth = CGFloat(floatLiteral: 0.5)
        titleButton.layer.borderColor = UIColor.white.cgColor
        titleButton.layer.cornerRadius = CGFloat(floatLiteral: 5.0)
        
        authorButton.backgroundColor = UIColor.black
        authorButton.layer.borderWidth = CGFloat(floatLiteral: 0.5)
        authorButton.layer.borderColor = UIColor.white.cgColor
        authorButton.layer.cornerRadius = CGFloat(floatLiteral: 5.0)
        
        dateButton.backgroundColor = UIColor.black
        dateButton.layer.borderWidth = CGFloat(floatLiteral: 0.5)
        dateButton.layer.borderColor = UIColor.white.cgColor
        dateButton.layer.cornerRadius = CGFloat(floatLiteral: 5.0)
        
        websiteButton.backgroundColor = UIColor.black
        websiteButton.layer.borderWidth = CGFloat(floatLiteral: 0.5)
        websiteButton.layer.borderColor = UIColor.white.cgColor
        websiteButton.layer.cornerRadius = CGFloat(floatLiteral: 5.0)
    }

    // Sort actions constructor

    @IBAction func onDateSortClick(_ sender: Any) {
        sortingOnProcess = true
        mainStore.dispatch(SortArticleByAction(SortType.date))
    }
    
    @IBAction func onTitleSortClick(_ sender: Any) {
        sortingOnProcess = true
        mainStore.dispatch(SortArticleByAction(SortType.title))
    }
    
    @IBAction func onWebsiteSortClick(_ sender: Any) {
        sortingOnProcess = true
        mainStore.dispatch(SortArticleByAction(SortType.website))
    }
    
    @IBAction func onAuthorSortClick(_ sender: Any) {
        sortingOnProcess = true
        mainStore.dispatch(SortArticleByAction(SortType.author))
    }
    
    @IBAction func onArrowClick(_ sender: Any) {
        sortingOnProcess = true
        mainStore.dispatch(ChangeSortOrderAction())
    }

    // Error alerts constructor

    func showAlertSyncError() {
        let alertController = UIAlertController(title: "Syncronization error",
                message: "Some downloaded articles could not be saved in your device.",
                preferredStyle: UIAlertControllerStyle.alert)

        alertController.addAction(UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default,handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }

    func showAlertMarkReadError() {
        let alertController = UIAlertController(title: "Mark read error",
                message: "Error marking this article as already read.",
                preferredStyle: UIAlertControllerStyle.alert)

        alertController.addAction(UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default,handler: nil))
        self.present(alertController, animated: true, completion: nil)

    }

}
