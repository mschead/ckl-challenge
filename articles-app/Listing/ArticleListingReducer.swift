//
// Created by Marcos Schead on 15/12/17.
// Copyright (c) 2017 Marcos Schead. All rights reserved.
//

import Foundation
import ReSwift

struct ArticleListingReducer: Reducer {

    typealias ReducerStateType = ArticleListingState

    func handleAction(action: Action, state: ArticleListingState?) -> ArticleListingState {
        if state == nil {
            return ArticleListingState()
        }
        
        switch action {
            case _ as ChangeSortOrderAction:
                return changeSortOrder(state: state!)
            case let action as SortArticleByAction:
                return sortBy(state: state!, action: action)
            case let action as CheckButtonClickedAction:
                return markArticleRead(state: state!, action: action)
            case let _ as ErrorMarkingReadAction:
                return setErrorMarkingRead(state: state!)
            default:
                return state!
        }
    }

    fileprivate func sortBy(state: ArticleListingState, action: SortArticleByAction) -> ArticleListingState {
        var state = state
        state.sortType = action.sortType
        return state
    }

    fileprivate func changeSortOrder(state: ArticleListingState) -> ArticleListingState {
        var state = state
        state.sortArrowUp = !state.sortArrowUp
        return state
    }

    fileprivate func markArticleRead(state: ArticleListingState, action: CheckButtonClickedAction) -> ArticleListingState {
        ArticlePersistence().updateMarkByTitle(title: action.article.title, mark_read: !action.article.markRead)
        return state
    }

    fileprivate func setErrorMarkingRead(state: ArticleListingState) -> ArticleListingState {
        var state = state
        state.errorMarkingRead = true
        return state
    }

}