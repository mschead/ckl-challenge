//
// Created by Marcos Schead on 16/12/17.
// Copyright (c) 2017 Marcos Schead. All rights reserved.
//

import Foundation

class DateUtils {

    static func convertStringToDate(date: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "mm-dd-yyyy" //Your date format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
        return dateFormatter.date(from: date)! //according to date format your date string
    }

}