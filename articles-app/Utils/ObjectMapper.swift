//
// Created by Marcos Schead on 18/12/17.
// Copyright (c) 2017 Marcos Schead. All rights reserved.
//

import Foundation
import CoreData

class ObjectMapper {

    static func NSObjectToArticle(object: Article) -> ArticleWrapper {
        let title = object.title ?? ""
        let website = object.website ?? ""
        let authors = object.authors ?? ""
        let date = object.date ?? ""
        let content = object.content ?? ""
        let image_url = object.image_url ?? ""
        let markRead = object.mark_read

        return ArticleWrapper(title: title, website: website, authors: authors, date: date, content: content, tags: [], image_url: image_url, markRead: markRead)
    }

}

