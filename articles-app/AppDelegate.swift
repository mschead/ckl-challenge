//
//  AppDelegate.swift
//  articles-app
//
//  Created by Marcos Schead on 15/12/17.
//  Copyright © 2017 Marcos Schead. All rights reserved.
//

import UIKit
import ReSwift
import CoreData
import MagicalRecord

var mainStore: Store<RootState>!

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        // initialize RootState of the application
        mainStore = Store(reducer: RootReducer(), state: RootState())

        // initialize MagicalRecord, an alternative to pure Core Data
        MagicalRecord.setupCoreDataStack(withStoreNamed: "Model")

        return true
    }

}

