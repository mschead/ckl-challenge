//
// Created by Marcos Schead on 15/12/17.
// Copyright (c) 2017 Marcos Schead. All rights reserved.
//

import Foundation

import Foundation
import ReSwift

struct RootState : StateType {

    var articleListingState: ArticleListingState = ArticleListingState()
    var articleDetailState: ArticleDetailState = ArticleDetailState()

}
