//
// Created by Marcos Schead on 15/12/17.
// Copyright (c) 2017 Marcos Schead. All rights reserved.
//

import Foundation

class ArticleRequest {

    func makeRequest(completion: ((Result<[ArticleWrapper]>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "cheesecakelabs.com"
        urlComponents.path = "/challenge"

        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.httpMethod = "GET"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                guard responseError == nil else {
                    completion?(.failure(responseError!))
                    return
                }

                guard let jsonData = responseData else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Data was not retrieved from request"]) as Error
                    completion?(.failure(error))
                    return
                }

//                Check if it's a JSON comingo, not a HTML page
//                let jsonString = String(data: jsonData, encoding: .utf8)
//                print(jsonString)

                let decoder = JSONDecoder()

                do {
                    let articles = try decoder.decode([ArticleWrapper].self, from: jsonData)
                    completion?(.success(articles))
                } catch {
                    completion?(.failure(error))
                }
            }
        }

        task.resume()
    }

}

enum Result<Value> {

    case success(Value)
    case failure(Error)
}