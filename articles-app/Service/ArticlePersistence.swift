//
// Created by Marcos Schead on 18/12/17.
// Copyright (c) 2017 Marcos Schead. All rights reserved.
//

import Foundation
import CoreData
import UIKit
import MagicalRecord

class ArticlePersistence {

    func save(article: ArticleWrapper, callbackError: @escaping () -> Void) {

        let articleDB = Article.mr_createEntity()!
        articleDB.setValue(article.title, forKeyPath: "title")
        articleDB.setValue(article.website, forKeyPath: "website")
        articleDB.setValue(article.authors, forKeyPath: "authors")
        articleDB.setValue(article.date, forKeyPath: "date")
        articleDB.setValue(article.content, forKeyPath: "content")
        articleDB.setValue(article.image_url, forKeyPath: "image_url")
        articleDB.setValue(article.markRead, forKeyPath: "mark_read")

        NSManagedObjectContext.mr_default().mr_saveToPersistentStore { success, error in
            if (error != nil) {
                callbackError()
            }
         }
    }

    func load() -> [NSManagedObject] {
        let context = NSManagedObjectContext.mr_default()
        return Article.mr_findAll(in: context) ?? []
    }

    func updateMarkByTitle(title: String, mark_read: Bool) {
        let context = NSManagedObjectContext.mr_default()
        let predicate = NSPredicate(format: "title = %@", title)
        let articles = Article.mr_findAll(with: predicate, in: context) ?? []
        
        for article in articles {
            article.setValue(mark_read, forKey: "mark_read")
        }

        NSManagedObjectContext.mr_default().mr_saveToPersistentStore { success, error in
            if (error != nil) {
                mainStore.dispatch(ErrorMarkingReadAction())
            }
        }
    }
}
